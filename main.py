# variables holding the values of inflation

inflat_Jan_1 = 1.59282448436825
inflat_Feb_1 = -0.453509101198007
inflat_Mar_1 = 2.32467171712441
inflat_Apr_1 = 1.26125440724877
inflat_May_1 = 1.78252628571251
inflat_Jun_1 = 2.32938454145522
inflat_Jul_1 = 1.50222984223283
inflat_Aug_1 = 1.78252628571251
inflat_Sept_1 = 2.32884899407637
inflat_Oct_1 = 0.616921348207244
inflat_Nov_1 = 2.35229588637833
inflat_Dec_1 = 0.337779545187098
inflat_Jan_2 = 1.57703524727525
inflat_Feb_2 = -0.292781442607648
inflat_Mar_2 = 2.48619659017508
inflat_Apr_2 = 0.267110317834564
inflat_May_2 = 1.41795267229799
inflat_Jun_2 = 1.05424326726375
inflat_Jul_2 = 1.4805201044812
inflat_Aug_2 = 1.57703524727525
inflat_Sept_2 = -0.0774206903147018
inflat_Oct_2 = 1.16573339872354
inflat_Nov_2 = -0.404186717638335
inflat_Dec_2 = 1.49970852083123

# variables taken from standard input

init_value = float(input("Podaj początkową wartość kredytu w zł: "))
instalment = float(input("Podaj ratę kredytu w zł: "))
loan_rate = float(input("Podaj oprocentowanie kredytu w %: "))

# the first year

loan_Jan_1 = (1+((inflat_Jan_1+loan_rate)/1200))*init_value-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Jan_1}, '
      f'to {init_value-loan_Jan_1} mniej w stosunku do początkowej kwoty')

loan_Feb_1 = (1+((inflat_Feb_1+loan_rate)/1200))*loan_Jan_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Feb_1}, '
      f'to {loan_Jan_1-loan_Feb_1} mniej niż w poprzednim miesiącu')

loan_Mar_1 = (1+((inflat_Mar_1+loan_rate)/1200))*loan_Feb_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Mar_1}, '
      f'to {loan_Feb_1-loan_Mar_1} mniej niż w poprzednim miesiącu')

loan_Apr_1 = (1+((inflat_Apr_1+loan_rate)/1200))*loan_Mar_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Apr_1}, '
      f'to {loan_Mar_1-loan_Apr_1} mniej niż w poprzednim miesiącu')

loan_May_1 = (1+((inflat_May_1+loan_rate)/1200))*loan_Apr_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_May_1}, '
      f'to {loan_Apr_1-loan_May_1} mniej niż w poprzednim miesiącu')

loan_Jun_1 = (1+((inflat_Jun_1+loan_rate)/1200))*loan_May_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Jun_1}, '
      f'to {loan_May_1-loan_Jun_1} mniej niż w poprzednim miesiącu')

loan_Jul_1 = (1+((inflat_Jul_1+loan_rate)/1200))*loan_Jun_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Jul_1}, '
      f'to {loan_Jun_1-loan_Jul_1} mniej niż w poprzednim miesiącu')

loan_Aug_1 = (1+((inflat_Aug_1+loan_rate)/1200))*loan_Jul_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Aug_1}, '
      f'to {loan_Jul_1-loan_Aug_1} mniej niż w poprzednim miesiącu')

loan_Sept_1 = (1+((inflat_Sept_1+loan_rate)/1200))*loan_Aug_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Sept_1}, '
      f'to {loan_Aug_1-loan_Sept_1} mniej niż w poprzednim miesiącu')

loan_Oct_1 = (1+((inflat_Oct_1+loan_rate)/1200))*loan_Sept_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Oct_1}, '
      f'to {loan_Sept_1-loan_Oct_1} mniej niż w poprzednim miesiącu')

loan_Nov_1 = (1+((inflat_Nov_1+loan_rate)/1200))*loan_Oct_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Nov_1}, '
      f'to {loan_Oct_1-loan_Nov_1} mniej niż w poprzednim miesiącu')

loan_Dec_1 = (1+((inflat_Dec_1+loan_rate)/1200))*loan_Nov_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Dec_1}, '
      f'to {loan_Nov_1-loan_Dec_1} mniej niż w poprzednim miesiącu')

# the second year

loan_Jan_2 = (1+((inflat_Jan_2+loan_rate)/1200))*loan_Dec_1-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Jan_2}, '
      f'to {loan_Dec_1-loan_Jan_2} mniej niż w poprzednim miesiącu')

loan_Feb_2 = (1+((inflat_Feb_2+loan_rate)/1200))*loan_Jan_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Feb_2}, '
      f'to {loan_Jan_2-loan_Feb_2} mniej niż w poprzednim miesiącu')

loan_Mar_2 = (1+((inflat_Mar_2+loan_rate)/1200))*loan_Feb_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Mar_2}, '
      f'to {loan_Feb_2-loan_Mar_2} mniej niż w poprzednim miesiącu')

loan_Apr_2 = (1+((inflat_Apr_2+loan_rate)/1200))*loan_Mar_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Apr_2}, '
      f'to {loan_Mar_2-loan_Apr_2} mniej niż w poprzednim miesiącu')

loan_May_2 = (1+((inflat_May_2+loan_rate)/1200))*loan_Apr_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_May_2}, '
      f'to {loan_Apr_2-loan_May_2} mniej niż w poprzednim miesiącu')

loan_Jun_2 = (1+((inflat_Jun_2+loan_rate)/1200))*loan_May_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Jun_2}, '
      f'to {loan_May_2-loan_Jun_2} mniej niż w poprzednim miesiącu')

loan_Jul_2 = (1+((inflat_Jul_2+loan_rate)/1200))*loan_Jun_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Jul_2}, '
      f'to {loan_Jun_2-loan_Jul_2} mniej niż w poprzednim miesiącu')

loan_Aug_2 = (1+((inflat_Aug_2+loan_rate)/1200))*loan_Jul_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Aug_2}, '
      f'to {loan_Jul_2-loan_Aug_2} mniej niż w poprzednim miesiącu')

loan_Sept_2 = (1+((inflat_Sept_2+loan_rate)/1200))*loan_Aug_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Sept_2}, '
      f'to {loan_Aug_2-loan_Sept_2} mniej niż w poprzednim miesiącu')

loan_Oct_2 = (1+((inflat_Oct_2+loan_rate)/1200))*loan_Sept_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Oct_2}, '
      f'to {loan_Sept_2-loan_Oct_2} mniej niż w poprzednim miesiącu')

loan_Nov_2 = (1+((inflat_Nov_2+loan_rate)/1200))*loan_Oct_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Nov_2}, '
      f'to {loan_Oct_2-loan_Nov_2} mniej niż w poprzednim miesiącu')

loan_Dec_2 = (1+((inflat_Dec_2+loan_rate)/1200))*loan_Nov_2-instalment
print(f'Twoja pozostała kwota kredytu to {loan_Dec_2}, '
      f'to {loan_Nov_2-loan_Dec_2} mniej niż w poprzednim miesiącu')